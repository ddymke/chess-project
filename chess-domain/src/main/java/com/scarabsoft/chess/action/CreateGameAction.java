package com.scarabsoft.chess.action;

import com.scarabsoft.chess.Player;

public class CreateGameAction extends BaseAction {

	public CreateGameAction() {
		super(ActionType.CreateGame, null, null, Player.BLACK);
	}

}
