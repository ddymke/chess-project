package com.scarabsoft.chess.database;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.scarabsoft.chess.json.JsonChessBoard;

public class ChessSource extends DataSource<JsonChessBoard> {

	public ChessSource(Context context) {
		super(context, "board", new String[] { "id", "gameId", "hash", "content", "last" });
	}

	@Override
	public String createTable() {
		return "create table " + tableName
				+ " (id integer primary key,gameId integer not null, hash text not null, content text not null, last text not null);";
	}

	@Override
	protected JsonChessBoard cursorToObject(Cursor cursor) {
		final JsonChessBoard result = new JsonChessBoard();
		result.setId(cursor.getInt(0));
		result.setGameId(cursor.getInt(1));
		result.setHash(cursor.getString(2));
		result.setContent(cursor.getString(3));
		result.setLastAction(cursor.getString(4));
		return result;
	}

	@Override
	public void insert(JsonChessBoard t) {
		final ContentValues values = new ContentValues();
		values.put(columns[1], t.getGameId());
		values.put(columns[2], t.getHash());
		values.put(columns[3], t.getContent());
		values.put(columns[4], t.getLastAction());
		database.insert(tableName, null, values);
	}

	@Override
	public JsonChessBoard delete(JsonChessBoard t) {
		return null;
	}

	public List<JsonChessBoard> getGame(int gameId) {
		List<JsonChessBoard> result = get("gameId = " + gameId);
		return result;
	}

	public JsonChessBoard getLastChessBoard(int gameId) {
		final List<JsonChessBoard> list = getGame(gameId);
		JsonChessBoard result = list.get(0);
		for (JsonChessBoard board : list) {
			if (board.getId() > result.getId()) {
				result = board;
			}
		}
		return result;
	}
}
