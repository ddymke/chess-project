package com.scarabsoft.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.scarabsoft.chess.util.ChessUtil;

public class JsonResponseWriter {

	public static void write(Object result, HttpServletResponse response) throws IOException {
		write(result, response, "application/json");
	}

	public static void write(Object result, HttpServletResponse response, String contentType) throws IOException {
		if (result == null) {
			response.setContentType(contentType);
			response.flushBuffer();
		} else {
			String json = ChessUtil.toJson(result);
			response.setContentType(contentType);
			response.getWriter().write(json);
			response.flushBuffer();
		}
	}

	public static void writeString(String result, HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		if (result != null) {
			response.getWriter().write(result);
			response.flushBuffer();
		}
		response.flushBuffer();
	}
}