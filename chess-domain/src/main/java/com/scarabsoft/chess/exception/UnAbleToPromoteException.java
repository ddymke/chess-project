package com.scarabsoft.chess.exception;

public class UnAbleToPromoteException extends BaseChessException {

	private static final long serialVersionUID = 8630016271090158000L;

	public UnAbleToPromoteException() {
		super(ChessErrorCode.UNABLE_TO_PROMOTE, "unable to promote piece");

	}

}
