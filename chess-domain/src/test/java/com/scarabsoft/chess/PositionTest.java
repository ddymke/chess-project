package com.scarabsoft.chess;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

import com.scarabsoft.chess.Position;

public class PositionTest {

	@Test
	public void fromStringToPosition() {
		Position position = new Position("A1");
		Assert.assertEquals(1, position.getX());
		Assert.assertEquals(1, position.getY());

		position = new Position("H8");
		Assert.assertEquals(8, position.getX());
		Assert.assertEquals(8, position.getY());
	}

	@Test
	public void fromCoordsToPosition() {
		Position position = new Position(1, 1);
		Assert.assertTrue("A1".equals(position.getFieldName()));
		position = new Position(8, 8);
		Assert.assertTrue("H8".equals(position.getFieldName()));
	}

	@Test
	public void testCoordsWorking() {
		Map<Position, String> map = new HashMap<Position, String>();
		map.put(new Position("A1"), "A1");
		map.put(new Position("H8"), "H8");
		Assert.assertTrue(map.get(new Position("A1")).equals("A1"));
		Assert.assertFalse(map.get(new Position("H8")).equals("A1"));

		map.put(new Position(56), "new A1");
		Assert.assertFalse(map.get(new Position("A1")).equals("A1"));
	}

	@Test
	public void testGridPosition() {
		// test A1
		Position pos = new Position("A1");
		Assert.assertEquals(56, pos.getGridPosition());
		pos = new Position(1, 1);
		Assert.assertEquals(56, pos.getGridPosition());

		pos = new Position("B1");
		Assert.assertEquals(57, pos.getGridPosition());

		// test A8
		pos = new Position("A8");
		Assert.assertEquals(0, pos.getGridPosition());
		pos = new Position(1, 8);
		Assert.assertEquals(0, pos.getGridPosition());

		pos = new Position(56);
		Assert.assertTrue(pos.equals(new Position("A1")));
	}
}
