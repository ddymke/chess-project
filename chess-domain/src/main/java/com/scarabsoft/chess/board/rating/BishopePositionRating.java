package com.scarabsoft.chess.board.rating;

import com.scarabsoft.chess.piece.ChessPiece.Type;

public class BishopePositionRating extends BasePositionRating {
	private final static int[][] offset_white = { //
	{ -16, -16, -8, -8, -8, -8, -16, -16 }, //
			{ -16, -16, 4, 4, 4, 4, -16, -16 }, //
			{ -8, 2, 6, 6, 6, 6, 2, -8 },//
			{ -8, 2, 6, 6, 6, 6, 2, -8 },//
			{ -8, 2, 4, 4, 4, 4, 2, -8 },//
			{ -8, 2, 2, 2, 2, 2, 2, -8 },//
			{ -8, -8, 0, 0, 0, 0, -8, -8 },//
			{ -16, -8, -8, -8, -8, -8, -8, -16 } };

	private final static int[][] offset_black = {//
	{ -16, -8, -8, -8, -8, -8, -8, -16 },//
			{ -8, -8, 0, 0, 0, 0, -8, -8 },//
			{ -8, 2, 2, 2, 2, 2, 2, -8 },//
			{ -8, 2, 4, 4, 4, 4, 2, -8 },//
			{ -8, 2, 6, 6, 6, 6, 2, -8 },//
			{ -8, 2, 6, 6, 6, 6, 2, -8 },//
			{ -16, -16, 4, 4, 4, 4, -16, -16 }, //
			{ -16, -16, -8, -8, -8, -8, -16, -16 } };

	public BishopePositionRating() {
		super(offset_white, offset_black, Type.BISHOPE);
	}

}
