package com.scarabsoft.chess.board.rating;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.board.ChessBoard;

public interface Rating {

	public enum RatingType {
		KNIGHT, ROOK, QUEEN, BISHOPE, PAWN// , MATERIAL
	}

	public int rate(ChessBoard board, Player player);

}
