package com.scarabsoft.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.scarabsoft.application.ChessApplication;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.json.JsonChessBoard;
import com.scarabsoft.chess.util.ChessConverter;
import com.scarabsoft.chess.util.Urls;
import com.scarabsoft.util.JsonResponseWriter;

@Controller
public class ChessController {

	@Autowired
	private ChessApplication chessApplication;

	@RequestMapping(value = Urls.ACTION_EXECUTE)
	public void executeAction(@PathVariable("id") int gameId, @RequestParam(value = "action") String action, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		try {
			BaseAction result = chessApplication.execute(gameId, ChessConverter.toAction(action));
			JsonResponseWriter.writeString(ChessConverter.fromAction(result), response);
		} catch (BaseChessException e) {
			JsonResponseWriter.write(e, response);
		}
	}

	@RequestMapping(value = Urls.GAME_CREATE)
	public void createNewGame(HttpServletResponse response) throws IOException {
		JsonChessBoard cBoard = chessApplication.createNewGame();
		JsonResponseWriter.write(cBoard, response);
	}

	@RequestMapping(value = Urls.GAME_SHOW)
	public void showBoard(@PathVariable("id") int id, HttpServletResponse response) throws IOException {
		JsonChessBoard model = chessApplication.getChessBoard(id);
		JsonResponseWriter.write(model, response);
	}

	public void syncGame(@PathVariable("id") int gameId, HttpServletResponse response) throws IOException {
		JsonChessBoard model = chessApplication.getChessBoard(gameId);
		JsonResponseWriter.writeString(model.getHash(), response);
	}

	@RequestMapping(value = Urls.LAST_ACTION)
	public void getLastAction(@PathVariable("id") int id, HttpServletResponse response) throws IOException {
		JsonChessBoard board = chessApplication.getChessBoard(id);
		JsonResponseWriter.writeString(board.getLastAction(), response);
	}

}
