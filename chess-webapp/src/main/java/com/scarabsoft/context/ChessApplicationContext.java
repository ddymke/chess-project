package com.scarabsoft.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.scarabsoft.application.ChessApplication;
import com.scarabsoft.application.ChessApplicationImpl;
import com.scarabsoft.application.StoreApplication;
import com.scarabsoft.application.StoreApplicationImpl;
import com.scarabsoft.repository.ChessRepository;
import com.scarabsoft.repository.HibernateChessRepository;
import com.scarabsoft.repository.HibernateStoreRepository;
import com.scarabsoft.repository.StoreRepository;

@PropertySource("classpath:chess.properties")
@ComponentScan(basePackages = "com.scarabsoft.controller")
@Import({ DataConfig.class })
@Configuration
@EnableAsync
public class ChessApplicationContext extends WebMvcConfigurationSupport implements ApplicationContextAware {

	@Autowired
	private Environment env;

	@Autowired
	private DataConfig dataConfig;

	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(10);
		executor.setWaitForTasksToCompleteOnShutdown(true);
		return executor;
	}

	@Bean
	public ChessRepository chessRepository() {
		return new HibernateChessRepository(dataConfig.sessionFactory());
	}

	@Bean
	public ChessApplication chessApplication() {
		return new ChessApplicationImpl();
	}

	@Bean
	public StoreRepository storeRepository() {
		return new HibernateStoreRepository(dataConfig.sessionFactory());
	}

	@Bean
	public StoreApplication storeApplication() {
		return new StoreApplicationImpl(storeRepository());
	}
}
