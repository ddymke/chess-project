package com.scarabsoft.chess.ai;
//package com.scarabsoft.chess.ai;
//
//import java.util.List;
//
//import com.scarabsoft.chess.Move;
//import com.scarabsoft.chess.Player;
//import com.scarabsoft.chess.action.MoveAction;
//import com.scarabsoft.chess.board.ChessBoard;
//
//public class MiniMax {
//
//	private AIUtil ave;
//
//	private ChessBoard board;
//
//	private int gameId;
//
//	public MiniMax(ChessBoard board, int gameId) {
//		ave = new AIUtil();
//		this.board = board;
//		this.gameId = gameId;
//	}
//
//	public Move findBestMove(int depth, Player player, boolean isHuman) {
//		List<Move> possibleMoves = ave.calculatePossibleMoves(board, player);
//		if (possibleMoves.size() > 0) {
//			int maxpos = -1; // pc point of view
//			int minpos = 1; // gamer point of view
//			for (int i = 0; i < possibleMoves.size(); i++) {
//				ChessBoard temp = (ChessBoard) board.cloneBoard();
//				MoveAction action = new MoveAction(possibleMoves.get(i), gameId);
//				temp.execute(action);
//				if (depth == 1) {
//					possibleMoves.get(i).setValue(temp.calculateValue(player));
//				} else {
//					Move bestMove = findBestMove(depth - 1, (player == Player.WHITE) ? Player.BLACK : Player.WHITE, (isHuman == true) ? false : true);
//					possibleMoves.get(i).setValue(bestMove.getValue());
//				}
//				if (i == 0) {
//					maxpos = minpos = 0;
//				} else {
//					int value = possibleMoves.get(i).getValue();
//					if (value > possibleMoves.get(maxpos).getValue()) {
//						maxpos = i;
//					} else if (value < possibleMoves.get(minpos).getValue()) {
//						minpos = i;
//					}
//				}
//			}
//			if (!isHuman) {
//				return possibleMoves.get(maxpos);
//			} else {
//				return possibleMoves.get(minpos);
//			}
//		} else {
//			// TODO check
//			// Handle Ôo
//			Move result = new Move();
//			if (depth == 1) {
//				result.setValue(board.calculateValue(player));
//			} else {
//				result.setValue(findBestMove(depth - 1, (player == Player.WHITE) ? Player.BLACK : Player.WHITE, (isHuman == true) ? false : true).getValue());
//			}
//			return result;
//		}
//	}
// }
