package com.scarabsoft.chess.network.domain;

public class NetworkResult {

	public static final int OK = 0;

	public static final int INV_AUTH = 1;

	public static final int CONNECTION_LOST = 2;

	public static final int TIME_OUT = 3;
}
