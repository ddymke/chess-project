package com.scarabsoft.chess.piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

//TODO give Pawn an offset
public class Pawn extends ChessPiece {

	public Pawn(Position position, Player player) {
		super(position, "pawn", 10, player, null, 0, Type.PAWN);
	}

	@Override
	public List<Position> getPossibleMoves(Position position, Map<Position, ChessPiece> fields) {
		final List<Position> result = new ArrayList<Position>();
		try {
			ChessPiece tempPiece = null;
			int dy = (player == Player.WHITE) ? 1 : -1;
			tempPiece = fields.get((new Position(position.getX(), position.getY() + dy)));
			if (tempPiece == null) {
				result.add(new Position(position.getX(), position.getY() + dy));
				if (moved == false) {
					tempPiece = fields.get(new Position(position.getX(), position.getY() + dy * 2));
					if (tempPiece == null) {
						result.add(new Position(position.getX(), position.getY() + dy * 2));
					}
				}
			}

			tempPiece = fields.get((new Position(position.getX() + 1, position.getY() + dy)));
			if (tempPiece != null && !tempPiece.getPlayer().equals(player)) {
				result.add(new Position(position.getX() + 1, position.getY() + dy));
			}

			tempPiece = fields.get((new Position(position.getX() - 1, position.getY() + dy)));
			if (tempPiece != null && !tempPiece.getPlayer().equals(player)) {
				result.add(new Position(position.getX() - 1, position.getY() + dy));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		return result;
	}
}
