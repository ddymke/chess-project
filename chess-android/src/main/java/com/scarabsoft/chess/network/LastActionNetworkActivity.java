package com.scarabsoft.chess.network;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntity;

import android.content.Intent;

import com.scarabsoft.chess.domain.ChessIPC;
import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.util.NetworkUtil;
import com.scarabsoft.chess.util.UrlUtil;
import com.scarabsoft.chess.util.Urls;

public class LastActionNetworkActivity extends BaseNetworkActivity {

	@Override
	protected HttpUriRequest getHttpUriRequest() {
		int gameId = getIntent().getExtras().getInt(ChessIPC.GAMEID);
		return NetworkUtil.generatePost(UrlUtil.replaceId(Urls.LAST_ACTION, gameId), new MultipartEntity());
	}

	@Override
	protected void onNetworkTaskSuccess(String content) {
		Intent intent = new Intent();
		intent.putExtra(ChessIPC.RESULT, content);
		setResult(NetworkResult.OK, intent);
	}

}
