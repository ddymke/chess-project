package com.scarabsoft.chess.piece;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public class Bishope extends ChessPiece {

	public Bishope(Position position, Player player) {
		super(position, "bishope", 30, player, new int[][] { { 1, 1 }, { 1, -1 }, { -1, 1 }, { -1, -1 } }, 8, Type.BISHOPE);
	}
}
