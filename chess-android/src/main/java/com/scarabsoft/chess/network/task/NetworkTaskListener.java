package com.scarabsoft.chess.network.task;

public interface NetworkTaskListener {

	void onNetworkTaskFinished(int result);

}
