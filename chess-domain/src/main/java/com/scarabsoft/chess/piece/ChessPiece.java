package com.scarabsoft.chess.piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public abstract class ChessPiece implements Comparable<ChessPiece> {

	public enum Type {
		PAWN, KNIGHT, BISHOPE, ROOK, QUEEN, KING;
	}

	protected String filename;

	protected String piece;

	protected int value;

	protected int[][] offsets;

	protected boolean moved = false;

	protected Player player;

	protected int loop;

	protected Type type;

	protected Position position;

	public ChessPiece() {
	}

	public ChessPiece(Position position, String piece, int value, Player player, int[][] offsets, int loop, Type type) {
		this.position = position;
		this.piece = piece;
		this.filename = player.toString() + "_" + piece;
		this.value = value;
		this.player = player;
		this.offsets = offsets;
		this.type = type;
		this.loop = loop;
	}

	/**
	 * @param position
	 * @return list of all possible positions to move
	 * */
	public List<Position> getPossibleMoves(Position position, Map<Position, ChessPiece> fields) {
		final List<Position> result = new ArrayList<Position>();
		for (int j = 0; j < offsets.length; j++) {
			for (int i = 1; i <= loop; i++) {
				int[] o = offsets[j];
				try {
					Position pos = new Position(position.getX() + o[0] * i, position.getY() + o[1] * i);
					ChessPiece piece = fields.get(pos);
					if (piece != null) {
						if (!piece.getPlayer().equals(player)) {
							result.add(pos);
						}
						break;
					}
					result.add(pos);
				} catch (ArrayIndexOutOfBoundsException e) {
				}
			}
		}
		return result;
	}

	public String getFilename() {
		return filename;
	}

	public int getValue() {
		return value;
	}

	public boolean isMoved() {
		return moved;
	}

	public Player getPlayer() {
		return player;
	}

	public int[][] getOffsets() {
		return offsets;
	}

	public String getPiece() {
		return piece;
	}

	@Override
	public String toString() {
		return "[" + piece + "," + player.toString() + "]";
	}

	@Override
	public int compareTo(ChessPiece o) {
		if (o.getPlayer().equals(player) && o.getPiece().equals(piece) && o.getPosition().equals(position)) {
			return 0;
		}
		return -1;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	public Type getType() {
		return type;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
}
