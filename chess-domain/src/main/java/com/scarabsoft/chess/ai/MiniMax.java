package com.scarabsoft.chess.ai;

import java.util.List;

import com.scarabsoft.chess.Move;
import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.board.rating.ChessRating;

public class MiniMax {

	private final static class MinimaxCalculator {

		private int moveCount = 0;

		private long startTime;

		private long time;

		private Player minPlayer;

		private Player maxPlayer;

		private ChessBoard board;

		private final int MAX_POSSIBLE_STRENGTH = Integer.MAX_VALUE;

		private final int MIN_POSSIBLE_STRENGTH = Integer.MIN_VALUE;

		private ChessRating rating;

		public MinimaxCalculator(ChessBoard b, Player max, Player min) {
			board = b;
			maxPlayer = max;
			minPlayer = min;
			rating = new ChessRating();
		}

		public int getMoveCount() {
			return moveCount;
		}

		public long getTime() {
			return time;
		}

		public Move calculateMove(int depth) {
			startTime = System.currentTimeMillis();

			List<Move> moves = board.getPossibleMoves(maxPlayer);
			int maxStrength = MIN_POSSIBLE_STRENGTH;
			int maxIndex = 0;

			for (int i = 0; i < moves.size(); i++) {
				Move move = moves.get(i);
				if (board.move(move)) {
					moveCount++;

					int strength = expandMinNode(depth - 1, maxStrength);
					if (strength > maxStrength) {
						maxStrength = strength;
						maxIndex = i;
					}
					board.undoLastMove();
				}
			}

			time = System.currentTimeMillis() - startTime;
			System.out.print("MINIMAX: Number of moves tried:" + moveCount);
			System.out.println(" Time:" + time + " milliseconds");
			return moves.get(maxIndex);
		}

		private int expandMaxNode(int depth, int parentMinimum) {
			if (depth == 0) {
				return rating.rate(board, maxPlayer);
			} else if (board.isGameOver()) {
				return Integer.MAX_VALUE;
			}

			List<Move> moves = board.getPossibleMoves(maxPlayer);
			int maxStrength = MIN_POSSIBLE_STRENGTH;

			for (int i = 0; i < moves.size(); i++) {
				Move move = moves.get(i);
				if (board.move(move)) {
					moveCount++;
					int strength = expandMinNode(depth - 1, maxStrength);
					if (strength > parentMinimum) {
						board.undoLastMove();
						return strength;
					}
					if (strength > maxStrength) {
						maxStrength = strength;
					}
					board.undoLastMove();
				}

			}

			return maxStrength;

		}

		private int expandMinNode(int depth, int parentMaximum) {
			if (depth == 0) {
				return rating.rate(board, maxPlayer);
			} else if (board.isGameOver()) {
				return Integer.MAX_VALUE;
			}

			List<Move> moves = board.getPossibleMoves(minPlayer);
			int minStrength = MAX_POSSIBLE_STRENGTH;

			for (int i = 0; i < moves.size(); i++) {
				Move move = moves.get(i);
				if (board.move(move)) {
					moveCount++;
					int strength = expandMaxNode(depth - 1, minStrength);
					if (strength < parentMaximum) {
						board.undoLastMove();
						return strength;
					}
					if (strength < minStrength) {
						minStrength = strength;
					}
					board.undoLastMove();
				}
			}
			return minStrength;
		}
	}

	public static Move findBestMove(ChessBoard board, int depth, Player player) {
		MinimaxCalculator calculator = new MinimaxCalculator(board, player, (player == Player.WHITE) ? Player.BLACK : Player.WHITE);
		Move result = null;
		result = calculator.calculateMove(depth);
		return result;
	}

}
