package com.scarabsoft.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.scarabsoft.chess.json.JsonChessBoard;

public class HibernateChessRepository extends GenericHibernateRepo<JsonChessBoard> implements ChessRepository {

	public HibernateChessRepository(SessionFactory sessionFactory) {
		super(JsonChessBoard.class, sessionFactory);
	}

	@Override
	public JsonChessBoard get(int id) {
		Criteria maxIdCriteria = createCriteria();
		maxIdCriteria.add(Restrictions.eq("gameId", id));
		maxIdCriteria.setProjection(Projections.max("id"));
		Integer maxId = (Integer) maxIdCriteria.uniqueResult();

		Criteria criteria = createCriteria();
		criteria.add(Restrictions.eq("gameId", id));
		criteria.add(Restrictions.eq("id", maxId));
		return (JsonChessBoard) criteria.uniqueResult();
	}
}
