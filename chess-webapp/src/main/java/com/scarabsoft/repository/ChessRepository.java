package com.scarabsoft.repository;

import com.scarabsoft.chess.json.JsonChessBoard;

public interface ChessRepository {

	public void save(JsonChessBoard cb);

	public void update(JsonChessBoard cb);

	public JsonChessBoard get(int id);

}
