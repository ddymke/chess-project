package com.scarabsoft.chess.board.rating;

import com.scarabsoft.chess.piece.ChessPiece.Type;

public class RookPositionRating extends BasePositionRating {

	private final static int offset[][] = { //
	{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 }, //
			{ 0, 0, 4, 6, 6, 4, 0, 0 } };

	public RookPositionRating() {
		super(offset, offset, Type.ROOK);

	}

}
