package com.scarabsoft.async;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ChessAsyncTask implements Runnable {

	public ChessAsyncTask() {

	}

	@Override
	public void run() {

	}
}
