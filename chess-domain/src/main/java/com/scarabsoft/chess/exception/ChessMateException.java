package com.scarabsoft.chess.exception;

import com.scarabsoft.chess.action.BaseAction;

public class ChessMateException extends BaseChessException {

	private final BaseAction checkMateAction;

	private static final long serialVersionUID = -6632322092079545325L;

	public ChessMateException(BaseAction checkMateAction) {
		super(ChessErrorCode.CHECK_MATE, "checked Mate!");
		this.checkMateAction = checkMateAction;
	}

	public BaseAction getMove() {
		return checkMateAction;
	}
}
