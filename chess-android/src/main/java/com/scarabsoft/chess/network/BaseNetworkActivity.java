package com.scarabsoft.chess.network;

import org.apache.http.client.methods.HttpUriRequest;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.scarabsoft.chess.R;
import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.network.domain.TimeOutDialog;
import com.scarabsoft.chess.network.task.NetworkTaskListener;
import com.scarabsoft.chess.network.task.SingleNetworkTask;

public abstract class BaseNetworkActivity extends Activity implements NetworkTaskListener {

	protected String token;

	private Context context;

	private SingleNetworkTask networkTask;

	public final static String HOST = "http://192.168.178.32:8080/chess-webapp";

	public SingleNetworkTask getNetworkTask() {
		return networkTask;
	}

	public void returnResult(int resultCode, Intent intent) {
		setResult(resultCode, intent);
		finish();
	}

	public void finishWithResult(int resultCode) {
		finishWithResult(resultCode, null);
	}

	public void finishWithResult(int resultCode, Intent intent) {
		if (intent != null) {
			startActivity(intent);
		}
		setResult(resultCode);
		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = this;

		setContentView(R.layout.activity_network);
		setVisible(false);

		networkTask = new SingleNetworkTask(getHttpUriRequest(), this);
		networkTask.execute(new Void[0]);

	}

	protected abstract HttpUriRequest getHttpUriRequest();

	protected abstract void onNetworkTaskSuccess(String content);

	@Override
	public void onNetworkTaskFinished(int result) {

		if (result == NetworkResult.OK) {

			String content = networkTask.getResult();
			onNetworkTaskSuccess(content);
			finish();

		} else if (result == NetworkResult.TIME_OUT) {
			setResult(NetworkResult.TIME_OUT);
			TimeOutDialog dialog = new TimeOutDialog(context);
			dialog.show();
			dialog.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					startActivity(new Intent(context, PreferenceActivity.class));
					finish();
				}
			});
		} else if (result == NetworkResult.INV_AUTH) {

		} else if (result == NetworkResult.CONNECTION_LOST) {
			setResult(NetworkResult.CONNECTION_LOST);
			System.exit(-1);
		} else {

		}
	}

}
