package com.scarabsoft.chess.action;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.piece.ChessPiece;

public class PromoteAction extends BaseAction {

	public PromoteAction(String position, ChessPiece piece, Player player) {
		super(ActionType.Promote, position, piece.getPiece(), player);
	}

}
