package com.scarabsoft.chess.domain;

public class ChessIPC {

	public final static String GAMEID = "gameId";

	public final static String ACTION = "action";

	public static final String RESULT = "result";

}
