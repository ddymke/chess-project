package com.scarabsoft.chess.exception;

import com.scarabsoft.chess.Position;

public class NoChessPieceException extends BaseChessException {

	private static final long serialVersionUID = 7060105544318230604L;

	private final Position position;

	public NoChessPieceException(Position position) {
		super(ChessErrorCode.NOT_CHESSPIECE, "their is no chesspiece on position: " + position);
		this.position = position;
	}

	public Position getPosition() {
		return position;
	}
}
