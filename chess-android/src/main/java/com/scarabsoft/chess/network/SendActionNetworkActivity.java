package com.scarabsoft.chess.network;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Intent;

import com.scarabsoft.chess.domain.ChessIPC;
import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.util.NetworkUtil;
import com.scarabsoft.chess.util.UrlUtil;
import com.scarabsoft.chess.util.Urls;

public class SendActionNetworkActivity extends BaseNetworkActivity {

	@Override
	protected HttpUriRequest getHttpUriRequest() {
		int id = getIntent().getExtras().getInt(ChessIPC.GAMEID);
		String action = getIntent().getExtras().getString(ChessIPC.ACTION);
		MultipartEntity entity = new MultipartEntity();
		try {
			entity.addPart(ChessIPC.ACTION, new StringBody(action));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return NetworkUtil.generateGet(UrlUtil.replaceId(Urls.ACTION_EXECUTE, id), "?action=" + URLEncoder.encode(action));
	}

	@Override
	protected void onNetworkTaskSuccess(String content) {
		final Intent intent = new Intent();
		intent.putExtra(ChessIPC.RESULT, content);
		setResult(NetworkResult.OK, intent);
	}

}
