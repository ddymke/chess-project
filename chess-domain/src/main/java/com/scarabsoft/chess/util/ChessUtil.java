package com.scarabsoft.chess.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.json.JsonChessBoard;

public class ChessUtil {

	private static final Gson gson = new GsonBuilder().create();

	public static boolean isOnBoard(Position position) {
		if (position.getX() > 8 || position.getX() < 1 || position.getY() > 8 || position.getY() < 1) {
			return false;
		}
		return true;
	}

	public static String toJson(Object object) {
		return gson.toJson(object);
	}

	public static JsonChessBoard toJsonChessBoard(String json) {
		return gson.fromJson(json, new TypeToken<JsonChessBoard>() {
		}.getType());
	}

}
