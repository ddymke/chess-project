package com.scarabsoft.chess.action;

public enum ActionType {

	Move, Promote, CreateGame;

}
