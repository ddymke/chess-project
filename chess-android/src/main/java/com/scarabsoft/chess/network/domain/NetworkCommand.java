package com.scarabsoft.chess.network.domain;

public class NetworkCommand {

	public static final int LAST_ACTION = 0;

	public static final int ACTION = 1;

	public static final int CREATE_GAME = 2;

	public static final int SYNC = 3;

}
