package com.scarabsoft.chess.adapter;

import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.R;
import com.scarabsoft.chess.piece.ChessPiece;

public class ChessAdapter extends BaseAdapter {

	private Map<Position, ChessPiece> field;

	private Context context;

	public ChessAdapter(Context context, Map<Position, ChessPiece> pieces) {
		this.field = pieces;
		this.context = context;
	}

	// its a gui hack try to fixx on logic level
	@Override
	public int getCount() {
		// if (pieces.size() > 64)
		// return 64;
		// else
		return field.size();
	}

	@Override
	public Object getItem(int position) {
		return field.get(new Position(position));
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void put(Position key, ChessPiece piece) {
		field.put(key, piece);
	}

	public ChessPiece remove(Position key) {
		ChessPiece result = field.get(key);
		field.put(key, null);
		return result;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View squareContainerView = convertView;
		if (convertView == null) {
			squareContainerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.chess_field, null);
			ImageView squareView = (ImageView) squareContainerView.findViewById(R.id.background);
			if ((position + position / 8) % 2 == 1) {
				squareView.setImageResource(R.drawable.black);
			} else {
				squareView.setImageResource(R.drawable.white);
			}

			ChessPiece piece = field.get(new Position(position));
			if (piece != null) {
				ImageView pieceView = (ImageView) squareContainerView.findViewById(R.id.piece);
				int res = context.getResources().getIdentifier("com.scarabsoft.chess:drawable/" + piece.getFilename(), null, null);
				pieceView.setImageResource(res);

			}

		}
		return squareContainerView;
	}

	public Map<Position, ChessPiece> getPieces() {
		return field;
	}

	public void setPieces(Map<Position, ChessPiece> pieces) {
		this.field = pieces;
	}
}
