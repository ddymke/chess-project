package com.scarabsoft.repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class GenericHibernateRepo<T> {

	private final Class<T> clazz;

	private final SessionFactory sessionFactory;

	public GenericHibernateRepo(Class<T> clazz, SessionFactory sessionFactory) {
		this.clazz = clazz;
		this.sessionFactory = sessionFactory;
	}

	protected Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected Criteria createCriteria() {
		return sessionFactory.getCurrentSession().createCriteria(clazz);
	}

	public void save(T obj) {
		currentSession().save(obj);
	}

	public void update(T obj) {
		currentSession().update(obj);
	}

	public T get(int id) {
		return (T) currentSession().get(clazz, id);
	}

	public void delete(T obj) {
		currentSession().delete(obj);
	}

}
