package com.scarabsoft.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.scarabsoft.application.StoreApplication;
import com.scarabsoft.chess.json.Store;
import com.scarabsoft.chess.util.Urls;
import com.scarabsoft.util.JsonResponseWriter;

@Controller
public class StoreController {

	@Autowired
	private StoreApplication application;

	@RequestMapping(value = Urls.STORE_GET)
	public void get(@PathVariable("hash") String hash, HttpServletResponse response) throws IOException {
		Store result = application.get(hash);
		JsonResponseWriter.write(result, response);
	}

}
