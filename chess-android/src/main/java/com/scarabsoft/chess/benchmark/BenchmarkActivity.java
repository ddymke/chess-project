package com.scarabsoft.chess.benchmark;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;

import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.R;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.adapter.ChessAdapter;
import com.scarabsoft.chess.ai.Ave;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.piece.ChessPiece;

public class BenchmarkActivity extends Activity {

	protected GridView chessView;

	protected ChessAdapter chessAdapter;

	protected ChessBoard chessBoard;

	protected long time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chess);
		chessView = (GridView) findViewById(R.id.chessboard);
		Ave.init();
	}

	protected List<BaseAction> getActions() {
		List<BaseAction> result = new ArrayList<BaseAction>();
		// result.add(new MoveAction(new Move(new Position("A2"), new
		// Position("A3"), new Pawn(Player.WHITE)), -1));
		// result.add(new MoveAction(new Move(new Position("A3"), new
		// Position("A4"), new Pawn(Player.WHITE)), -1));
		return result;
	}

	protected void execute(BaseAction action) {
		try {
			chessBoard.execute(action);
		} catch (BaseChessException e) {
			e.printStackTrace();
		}
		drawField(chessBoard.getField());
	}

	protected void drawField(Map<Position, ChessPiece> map) {
		chessAdapter = new ChessAdapter(this, map);
		chessView.setAdapter(chessAdapter);
		chessAdapter.notifyDataSetChanged();
	}

	public long getTime() {
		return time;
	}
}
