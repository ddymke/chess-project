package com.scarabsoft.chess.exception;

public class NotUserTurnException extends BaseChessException {

	private static final long serialVersionUID = 3847721294790415888L;

	public NotUserTurnException() {
		super(ChessErrorCode.NOT_USER_TURN, "you are not allowed to execute action");

	}

}
