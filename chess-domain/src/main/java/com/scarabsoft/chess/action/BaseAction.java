package com.scarabsoft.chess.action;

import com.scarabsoft.chess.Player;

public abstract class BaseAction {

	protected final ActionType type;

	protected final String param1;

	protected final String param2;

	protected final Player player;

	public BaseAction(ActionType type, String param1, String param2, Player player) {
		this.type = type;
		this.param1 = param1;
		this.param2 = param2;
		this.player = player;
	}

	public ActionType getType() {
		return type;
	}

	public String getParam1() {
		return param1;
	}

	public String getParam2() {
		return param2;
	}

	public Player getPlayer() {
		return player;
	}
}
