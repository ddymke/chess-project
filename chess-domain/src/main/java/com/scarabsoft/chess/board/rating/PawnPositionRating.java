package com.scarabsoft.chess.board.rating;

import com.scarabsoft.chess.piece.ChessPiece.Type;

public class PawnPositionRating extends BasePositionRating {

	private final static int[][] offset_white = { //
	{ 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 28, 28, 35, 42, 45, 35, 28, 28 },//
			{ -9, -3, 7, 12, 15, 7, -3, -9 },//
			{ -10, -10, 6, 8, 10, 6, -11, -10 },//
			{ -11, -11, 4, 5, 6, 2, -11, -11 },//
			{ -11, -11, 0, 0, 1, 0, -11, -11 },//
			{ -6, -6, 4, 5, 5, 4, -6, -6 },//
			{ 0, 0, 0, 0, 0, 0, 0, 0 } };

	private final static int[][] offset_black = {//
	{ 0, 0, 0, 0, 0, 0, 0, 0 },//
			{ -6, -6, 4, 5, 5, 4, -6, -6 },//
			{ -11, -11, 0, 0, 1, 0, -11, -11 },//
			{ -11, -11, 4, 5, 6, 2, -11, -11 },//
			{ -10, -10, 6, 8, 10, 6, -11, -10 },//
			{ -9, -3, 7, 12, 15, 7, -3, -9 },//
			{ 28, 28, 35, 42, 45, 35, 28, 28 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0 } };

	public PawnPositionRating() {
		super(offset_white, offset_black, Type.PAWN);
	}

}
