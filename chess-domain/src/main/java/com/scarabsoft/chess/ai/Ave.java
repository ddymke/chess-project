package com.scarabsoft.chess.ai;

import com.scarabsoft.chess.Move;
import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.action.MoveAction;
import com.scarabsoft.chess.board.ChessBoard;

public class Ave {

	private static Ave instance;

	public static void init() {
		if (instance == null) {
			instance = new Ave();
		}
	}

	private Ave() {

	}

	public static Ave getInstance() {
		return instance;
	}

	public BaseAction calculateBestAction(int gameId, ChessBoard board, Player player) {
		Move result = MiniMax.findBestMove(board, dynamicDepth(board), player);
		return new MoveAction(result, gameId);
	}

	public int dynamicDepth(ChessBoard board) {
		return 3;
	}
}
