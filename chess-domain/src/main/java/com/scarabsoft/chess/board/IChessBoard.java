package com.scarabsoft.chess.board;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import com.scarabsoft.chess.Move;
import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.piece.ChessPiece;

public interface IChessBoard {

	public void execute(BaseAction action) throws BaseChessException;

	public Map<Position, ChessPiece> getChessPieces();

	public Map<Position, ChessPiece> getChessPieces(Player player);

	public List<Move> getPossibleMoves(Player player) throws BaseChessException;

	public String hashBoard() throws NoSuchAlgorithmException, UnsupportedEncodingException;

	public ChessBoard cloneBoard();

	public int calculateValue(Player player);

	// dont use it directly, just for MiniMax
	public void undoLastMove();

	public boolean isGameOver();

	public boolean move(Move m);

}
