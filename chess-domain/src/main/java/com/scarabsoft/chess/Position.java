package com.scarabsoft.chess;

public class Position implements Comparable<Position> {

	// android gridview
	public static final int[][] positions = { { 56, 48, 40, 32, 24, 16, 8, 0 }, { 57, 49, 41, 33, 25, 17, 9, 1 }, { 58, 50, 42, 34, 26, 18, 10, 2 },
			{ 59, 51, 43, 35, 27, 19, 11, 3 }, { 60, 52, 44, 36, 28, 20, 12, 4 }, { 61, 53, 45, 37, 29, 21, 13, 5 }, { 62, 54, 46, 38, 30, 22, 14, 6 },
			{ 63, 55, 47, 39, 31, 23, 15, 7 } };

	private int x;

	private int y;

	private String fieldName;

	private int gridPosition;

	public Position(int gridPosition) {
		this.gridPosition = gridPosition;
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (positions[x][y] == gridPosition) {
					this.x = x + 1;
					this.y = y + 1;
					break;
				}
			}
		}
		this.fieldName = (char) (x + 64) + Integer.toString(y);
	}

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		this.fieldName = (char) (x + 64) + Integer.toString(y);
		this.gridPosition = positions[x - 1][y - 1];
	}

	public Position(String fieldname) {
		this.fieldName = fieldname;
		this.x = (int) fieldname.charAt(0) - 64;
		this.y = Integer.valueOf(fieldName.charAt(1)) - 48;
		this.gridPosition = positions[x - 1][y - 1];
	}

	public String getFieldName() {
		return fieldName;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getGridPosition() {
		return gridPosition;
	}

	@Override
	public String toString() {
		return fieldName;
	}

	@Override
	public int compareTo(Position o) {
		if (o.getFieldName().equals(fieldName)) {
			return 0;
		} else {
			return -1;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Position && ((Position) obj).getFieldName().equals(fieldName)) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return gridPosition;
	}
}
