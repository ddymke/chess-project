package com.scarabsoft.chess.board;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.piece.Bishope;
import com.scarabsoft.chess.piece.ChessPiece;
import com.scarabsoft.chess.piece.King;
import com.scarabsoft.chess.piece.Rook;

public class ChessBoardTest {

	@Test
	public void checkTest() {
		Map<Position, ChessPiece> map = new HashMap<Position, ChessPiece>();
		map.put(new Position("A1"), new Rook(new Position("A1"), Player.WHITE));
		map.put(new Position("A8"), new King(new Position("A8"), Player.BLACK));
		map.put(new Position("H1"), new Bishope(new Position("H1"), Player.WHITE));
		ChessBoard c = new ChessBoard();
		c.setField(map);

		boolean whitePlayerChecked = c.isChecked(Player.WHITE);
		boolean blackPlayerChecked = c.isChecked(Player.BLACK);

		Assert.assertFalse(whitePlayerChecked);
		Assert.assertTrue(blackPlayerChecked);
	}
}
