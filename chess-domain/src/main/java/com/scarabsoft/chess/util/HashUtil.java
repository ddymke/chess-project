package com.scarabsoft.chess.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeSet;

import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.piece.ChessPiece;

public class HashUtil {

	public static String hash(Map<Position, ChessPiece> fields) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String temp = "";
		for (Position key : new TreeSet<Position>(fields.keySet()))
			temp += key.toString() + ":" + ChessConverter.fromChessPiece(fields.get(key)) + ";";
		return convertToHex(MessageDigest.getInstance("SHA-512").digest(temp.getBytes("UTF-8")));
	}

	private static String convertToHex(byte[] raw) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < raw.length; i++) {
			sb.append(Integer.toString((raw[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

}
