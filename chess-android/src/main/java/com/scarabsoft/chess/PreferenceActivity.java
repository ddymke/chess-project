package com.scarabsoft.chess;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;

public class PreferenceActivity extends android.preference.PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.pref);

		final EditTextPreference gameIdText = (EditTextPreference) findPreference("gameId");
		gameIdText.setTitle("Your current game id:  " + gameIdText.getText().toString());
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

	}

}
