package com.scarabsoft.chess.database;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class DataSource<T> {

	protected class SQLiteHelper extends SQLiteOpenHelper {

		public static final String DB_NAME = "chess.db";

		public static final int DB_VERSION = 1;

		public SQLiteHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(createTable());
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(dropTable());
			onCreate(db);
		}

	}

	protected SQLiteHelper helper;

	protected Context context;

	protected SQLiteDatabase database;

	protected final String tableName;

	protected final String[] columns;

	public abstract String createTable();

	protected abstract T cursorToObject(Cursor cursor);

	public abstract void insert(T t);

	public abstract T delete(T t);

	public DataSource(Context context, String tableName, String[] columns) {
		helper = new SQLiteHelper(context);
		this.context = context;
		this.tableName = tableName;
		this.columns = columns;
		open();
	}

	public String dropTable() {
		return "DROP TABLE IF EXISTS " + tableName;
	}

	public void open() throws SQLException {
		database = helper.getWritableDatabase();
	}

	public void close() {
		helper.close();
	}

	protected List<T> get(String selection) {
		List<T> result = new ArrayList<T>();
		final Cursor cursor = database.query(tableName, columns, selection, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			T t = cursorToObject(cursor);
			result.add(t);
			cursor.moveToNext();
		}
		cursor.close();
		return result;
	}

	public List<T> getAll() {
		return get(null);
	}

}
