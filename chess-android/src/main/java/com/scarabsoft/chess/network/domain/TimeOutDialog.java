package com.scarabsoft.chess.network.domain;

import android.app.AlertDialog;
import android.content.Context;

public class TimeOutDialog extends AlertDialog {

	private OnClickListener listener;

	public TimeOutDialog(Context context) {
		super(context);
		setTitle("Connection TimeOut!");
		setMessage("Please check your settings and try again.");
		setButton("Ok", listener);
	}

	public void setOnClickListener(OnClickListener listener) {
		this.listener = listener;
		setButton("Ok", listener);
	}

}
