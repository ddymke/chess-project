package com.scarabsoft.chess.piece;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public class King extends ChessPiece {

	public King(Position position, Player player) {
		super(position, "king", 10000, player, new int[][] { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, 1 }, { -1, -1 } }, 1,
				Type.BISHOPE);
	}
}
