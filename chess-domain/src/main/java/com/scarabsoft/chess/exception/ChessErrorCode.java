package com.scarabsoft.chess.exception;

public class ChessErrorCode {

	public static final int NOT_USER_TURN = 1;

	public static final int INVALID_MOVE = 2;

	public static final int NOT_CHESSPIECE = 3;

	public static final int UNABLE_TO_PROMOTE = 4;

	public static final int CHECK_EXCEPTION = 5;

	public static final int CHECK_MATE = 6;

}
