package com.scarabsoft.chess.util;

public class UrlUtil {

	public static String replaceId(String url, int id) {
		return url.replace("{id}", new Integer(id).toString());
	}

	public static String replaceId(String url, long id) {
		return url.replace("{id}", new Long(id).toString());
	}

	public static String replaceId(String url, String id) {
		return url.replace("{id}", id);
	}

}
