package com.scarabsoft.chess.network;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntity;

import android.content.Intent;

import com.scarabsoft.chess.domain.ChessIPC;
import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.util.NetworkUtil;
import com.scarabsoft.chess.util.Urls;

public class CreateGameNetworkActivity extends BaseNetworkActivity {

	@Override
	protected HttpUriRequest getHttpUriRequest() {
		return NetworkUtil.generatePost(Urls.GAME_CREATE, new MultipartEntity());
	}

	@Override
	protected void onNetworkTaskSuccess(String content) {
		Intent result = new Intent();
		result.putExtra(ChessIPC.RESULT, content);
		setResult(NetworkResult.OK, result);
	}

}
