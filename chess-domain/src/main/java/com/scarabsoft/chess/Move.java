package com.scarabsoft.chess;

import com.scarabsoft.chess.piece.ChessPiece;

public class Move implements Comparable<Move> {

	private Position src;

	private Position dest;

	private ChessPiece chessPiece;

	private int value;

	public Move() {

	}

	public Move(Position src, Position dest, ChessPiece chessPiece) {
		this.src = src;
		this.dest = dest;
		this.chessPiece = chessPiece;
	}

	public ChessPiece getChessPiece() {
		return chessPiece;
	}

	public Position getDest() {
		return dest;
	}

	public Position getSrc() {
		return src;
	}

	public void setChessPiece(ChessPiece chessPiece) {
		this.chessPiece = chessPiece;
	}

	public void setDest(Position dest) {
		this.dest = dest;
	}

	public void setSrc(Position src) {
		this.src = src;
	}

	@Override
	public String toString() {
		return "[" + chessPiece + ",src:" + src + ",dest:" + dest + "]";
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public int compareTo(Move o) {
		if (o.getSrc().equals(src) && o.getDest().equals(dest) && o.getChessPiece().equals(chessPiece)) {
			return 0;
		}
		return -1;
	}
}
