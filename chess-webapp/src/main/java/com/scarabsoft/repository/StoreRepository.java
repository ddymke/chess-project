package com.scarabsoft.repository;

import com.scarabsoft.chess.json.Store;

public interface StoreRepository {
	public void save(Store store);

	public void update(Store store);

	public Store get(int id);

	public Store get(String hash);
}
