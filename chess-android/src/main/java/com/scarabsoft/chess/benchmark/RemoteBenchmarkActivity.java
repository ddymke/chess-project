package com.scarabsoft.chess.benchmark;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.domain.ChessIPC;
import com.scarabsoft.chess.json.JsonChessBoard;
import com.scarabsoft.chess.network.CreateGameNetworkActivity;
import com.scarabsoft.chess.network.LastActionNetworkActivity;
import com.scarabsoft.chess.network.SendActionNetworkActivity;
import com.scarabsoft.chess.network.domain.NetworkCommand;
import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.util.ChessConverter;
import com.scarabsoft.chess.util.ChessUtil;

public class RemoteBenchmarkActivity extends BenchmarkActivity {

	private int gameId;

	private Timer timer;

	private int counter = 0;

	private long start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		start = System.currentTimeMillis();
		Intent intent = new Intent(this, CreateGameNetworkActivity.class);
		startActivityForResult(intent, NetworkCommand.CREATE_GAME);
	}

	private void getLastAction() {
		final Intent intent = new Intent(this, LastActionNetworkActivity.class);
		intent.putExtra(ChessIPC.GAMEID, gameId);
		startActivityForResult(intent, NetworkCommand.LAST_ACTION);
	}

	private void startTimer() {
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				getLastAction();
			}
		}, 3000, 6000);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		BaseAction action;
		if (NetworkResult.OK == resultCode) {
			if (requestCode == NetworkCommand.CREATE_GAME) {
				JsonChessBoard jBoard = ChessUtil.toJsonChessBoard(data.getExtras().getString(ChessIPC.RESULT));
				gameId = jBoard.getGameId();
				chessBoard = ChessConverter.fromJsonChessBoard(jBoard);
				drawField(chessBoard.getField());
				nextMove();
			} else if (requestCode == NetworkCommand.LAST_ACTION) {
				timer.cancel();
				action = ChessConverter.toAction(data.getExtras().getString(ChessIPC.RESULT));
				if (action.getPlayer() == Player.BLACK) {
					execute(action);
					nextMove();
				}
			} else if (requestCode == NetworkCommand.ACTION) {
				action = ChessConverter.toAction(data.getExtras().getString(ChessIPC.RESULT));
				execute(action);
				startTimer();
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	private void nextMove() {
		// white player
		if (counter < getActions().size()) {
			Intent intent = new Intent(this, SendActionNetworkActivity.class);
			intent.putExtra(ChessIPC.ACTION, ChessConverter.fromAction(getActions().get(counter++)));
			intent.putExtra(ChessIPC.GAMEID, gameId);
			startActivityForResult(intent, NetworkCommand.ACTION);
		} else {
			Toast.makeText(this, "Finished!", Toast.LENGTH_LONG).show();
			time = System.currentTimeMillis() - start;
		}
	}

}
