package com.scarabsoft.chess.exception;

public class ChessException extends BaseChessException {

	private static final long serialVersionUID = -3145482127466661019L;

	public ChessException() {
		super(ChessErrorCode.CHECK_EXCEPTION, "you are checked");
	}

}
