package com.scarabsoft.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.action.CreateGameAction;
import com.scarabsoft.chess.ai.Ave;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.exception.NotUserTurnException;
import com.scarabsoft.chess.json.JsonChessBoard;
import com.scarabsoft.chess.json.Store;
import com.scarabsoft.chess.util.ChessConverter;
import com.scarabsoft.repository.ChessRepository;
import com.scarabsoft.repository.StoreRepository;

/**
 * TODO computer is always black.. change it
 * 
 */
@Transactional
public class ChessApplicationImpl implements ChessApplication {

	// @Autowired
	// private ApplicationContext context;

	@Autowired
	private ChessRepository chessRepository;

	@Autowired
	private StoreRepository storeRepository;

	public ChessApplicationImpl() {
		Ave.init();
	}

	@Override
	public JsonChessBoard createNewGame() {
		ChessBoard chessBoard = new ChessBoard();
		try {
			chessBoard.execute(new CreateGameAction());
		} catch (BaseChessException e) {
			e.printStackTrace();
		}
		JsonChessBoard result = ChessConverter.toJsonChessBoad(chessBoard);
		chessRepository.save(result);
		result.setGameId(result.getId());
		chessRepository.update(result);
		return result;
	}

	@Override
	public JsonChessBoard getChessBoard(int gameId) {
		JsonChessBoard cb = chessRepository.get(gameId);
		return cb;
	}

	@Override
	public BaseAction execute(int gameId, BaseAction action) throws BaseChessException {
		ChessBoard board = ChessConverter.fromJsonChessBoard(chessRepository.get(gameId));
		if (board.getLastAction().getPlayer() == Player.WHITE) {
			throw new NotUserTurnException();
		}
		switch (action.getType()) {
		case CreateGame:
			createNewGame();
			break;
		case Promote:
		case Move:
			board.execute(action);
			chessRepository.save(ChessConverter.toJsonChessBoad(board));
			Store tempStore = storeRepository.get(board.getHash());
			if (tempStore != null) {
				BaseAction bestAction = ChessConverter.toAction(tempStore.getBestAction());
				board.execute(bestAction);
				chessRepository.save(ChessConverter.toJsonChessBoad(board));
			} else {
				BaseAction bestMove = Ave.getInstance().calculateBestAction(gameId, board, Player.BLACK);
				board = ChessConverter.fromJsonChessBoard(chessRepository.get(gameId));
				board.execute(bestMove);
				System.out.println("BestMove:" + bestMove);
				chessRepository.save(ChessConverter.toJsonChessBoad(board));

				final Store store = ChessConverter.ChessBoardToStore(board, "black");
				storeRepository.save(store);

			}
			break;

		default:
		}
		System.out.println("FINISHED!");
		return action;
	}
}
