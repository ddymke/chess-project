package com.scarabsoft.chess.util;

public class Urls {

	// ChessController

	public final static String LAST_ACTION = "/last/{id}";

	public final static String ACTION_EXECUTE = "/action/{id}";

	public final static String GAME_CREATE = "/game/create";

	public final static String GAME_SHOW = "/game/{id}";

	public final static String GAME_SYNC = "/game/sync/{id}";

	// UserController

	// StoreController

	public final static String STORE_GET = "/store/get/{hash}";

}
