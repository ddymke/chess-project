package com.scarabsoft.application;

import org.springframework.transaction.annotation.Transactional;

import com.scarabsoft.chess.json.Store;
import com.scarabsoft.repository.StoreRepository;

@Transactional
public class StoreApplicationImpl implements StoreApplication {

	private final StoreRepository storeRepository;

	public StoreApplicationImpl(StoreRepository repo) {
		this.storeRepository = repo;
	}

	@Override
	public Store get(String hash) {
		Store result = storeRepository.get(hash);
		return result;
	}

}
