package com.scarabsoft.chess.action;

import com.scarabsoft.chess.Move;
import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public class MoveAction extends BaseAction {

	public MoveAction(int gameId, Position source, Position destination, Player player) {
		super(ActionType.Move, source.toString(), destination.toString(), player);
	}

	public MoveAction(Move move, Integer gameId) {
		super(ActionType.Move, move.getSrc().toString(), move.getDest().toString(), move.getChessPiece().getPlayer());
	}

	@Override
	public String toString() {
		return "[" + param1 + "," + param2 + "," + player.toString() + "]";
	}
}
