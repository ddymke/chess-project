package com.scarabsoft.chess.board;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.scarabsoft.chess.Move;
import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.action.MoveAction;
import com.scarabsoft.chess.action.PromoteAction;
import com.scarabsoft.chess.board.rating.ChessRating;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.exception.NoChessPieceException;
import com.scarabsoft.chess.exception.UnAbleToPromoteException;
import com.scarabsoft.chess.piece.Bishope;
import com.scarabsoft.chess.piece.ChessPiece;
import com.scarabsoft.chess.piece.ChessPiece.Type;
import com.scarabsoft.chess.piece.King;
import com.scarabsoft.chess.piece.Knight;
import com.scarabsoft.chess.piece.Pawn;
import com.scarabsoft.chess.piece.Queen;
import com.scarabsoft.chess.piece.Rook;
import com.scarabsoft.chess.util.ChessUtil;
import com.scarabsoft.chess.util.HashUtil;

public class ChessBoard implements IChessBoard {

	private int gameId;

	private String hash;

	private Map<Position, ChessPiece> field;

	private BaseAction lastAction;

	private boolean ended = false;

	private final ChessRating rating = new ChessRating();

	public ChessBoard() {
		field = new HashMap<Position, ChessPiece>();
	}

	@Override
	public void execute(BaseAction action) throws BaseChessException {
		switch (action.getType()) {
		case CreateGame:
			initNewGame();
			break;
		case Move:
			move((MoveAction) action);
			break;
		case Promote:
			promote((PromoteAction) action);
			break;
		default:

		}
		lastAction = action;
		try {
			hash = HashUtil.hash(field);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<Position, ChessPiece> getChessPieces() {
		return field;
	}

	private Map<Position, ChessPiece> getKingAttackingPieces(Player player) {
		final Map<Position, ChessPiece> result = new HashMap<Position, ChessPiece>();
		Position kingPosition = getKingPosition(player);
		for (Position position : getChessPieces(getOponentPlayer(player)).keySet()) {
			ChessPiece piece = field.get(position);
			if (piece != null && piece.getPossibleMoves(position, field).contains(kingPosition)) {
				result.put(position, piece);
			}
		}
		return result;
	}

	public List<Move> getPossibleMoves(Player player) {
		List<Move> result = new ArrayList<Move>();
		if (isChecked(player)) {
			// can king move?
			Position kingPosition = getKingPosition(player);
			ChessPiece king = field.get(kingPosition);
			List<Position> kingPositions = king.getPossibleMoves(kingPosition, field);
			for (Position position : kingPositions) {
				final Move move = new Move(kingPosition, position, king);
				if (isCheckedMove(player, move) == false) {
					result.add(move);
				}
			}

			Map<Position, ChessPiece> attackingMap = getKingAttackingPieces(player);
			for (Position attackingPosition : attackingMap.keySet()) {
				// case 1 kill enemy
				Map<Position, ChessPiece> pieces = getChessPieces(player);
				for (Position position : pieces.keySet()) {
					ChessPiece piece = field.get(position);
					if (piece.getPossibleMoves(position, field).contains(attackingPosition)) {
						result.add(new Move(position, attackingPosition, piece));
					}
				}
				// case 2 block
				// TODO implement
			}

		} else if (isPat(player)) {
			ended = true;
		} else {
			final Map<Position, ChessPiece> field = getChessPieces(player);
			for (Position key : field.keySet()) {
				ChessPiece piece = field.get(key);
				if (piece != null) {
					for (Position pos : piece.getPossibleMoves(key, field)) {
						final Move move = new Move(key, pos, piece);
						if (isCheckedMove(player, move) == false) {
							result.add(move);
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	public Map<Position, ChessPiece> getChessPieces(Player player) {
		Map<Position, ChessPiece> result = new HashMap<Position, ChessPiece>();
		for (Position key : field.keySet()) {
			ChessPiece piece = field.get(key);
			if (piece != null && piece.getPlayer().equals(player)) {
				result.put(key, piece);
			}
		}
		return result;
	}

	public List<Position> getPossiblePositions(ChessPiece piece, Position position) throws BaseChessException {
		if (piece != null) {
			return piece.getPossibleMoves(position, field);
		} else {
			throw new NoChessPieceException(position);
		}
	}

	private Position getKingPosition(Player player) {
		Position result = null;
		Map<Position, ChessPiece> playerPieces = getChessPieces(player);
		for (Position key : playerPieces.keySet()) {
			ChessPiece piece = playerPieces.get(key);
			if (piece != null && piece.getPiece().equals("king")) {
				result = key;
			}
		}
		return result;
	}

	public boolean isPat(Player player) {
		// TODO implement
		return false;
	}

	public boolean isCheckedMove(Player player, Move move) {
		ChessBoard temp = cloneBoard();
		try {
			temp.execute(new MoveAction(move, 0));
			if (temp.getKingAttackingPieces(player).size() > 0) {
				return true;
			}
		} catch (BaseChessException e) {
			e.printStackTrace();
		}
		return false;
	}

	private Player getOponentPlayer(Player player) {
		return (player == Player.WHITE) ? Player.BLACK : Player.WHITE;
	}

	public int size() {
		return field.size();
	}

	public boolean isChecked(Player player) {
		Position kingPosition = getKingPosition(player);
		Player oponent = getOponentPlayer(player);
		Map<Position, ChessPiece> oponentPieces = getChessPieces(oponent);
		for (Position key : oponentPieces.keySet()) {
			ChessPiece piece = oponentPieces.get(key);
			if (piece != null) {
				for (Position pos : piece.getPossibleMoves(key, field)) {
					if (pos.equals(kingPosition)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean isCheckedMate(Player player) {

		return false;
	}

	private void promote(PromoteAction action) throws BaseChessException {
		final Position position = new Position(action.getParam1());
		ChessPiece piece = field.get(position);
		if (!ChessUtil.isOnBoard(position) || piece == null || !piece.getPiece().equals("pawn")) {
			throw new UnAbleToPromoteException();
		}
		field.put(position, new Queen(position, action.getPlayer()));
	}

	@Override
	public String hashBoard() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return HashUtil.hash(field);
	}

	@Override
	public ChessBoard cloneBoard() {
		ChessBoard result = new ChessBoard();
		result.setField(new HashMap<Position, ChessPiece>(field));
		result.setGameId(gameId);
		result.setHash(hash);
		result.setLastAction(lastAction);
		return result;
	}

	private void move(MoveAction action) {
		ChessPiece piece = field.get(new Position(action.getParam1()));
		// TODO handle null fields..
		if (piece != null) {
			field.put(new Position(action.getParam1()), null);
			piece.setMoved(true);
			piece.setPosition(new Position(action.getParam2()));
			field.put(new Position(action.getParam2()), piece);
		}
	}

	private void addPieceToBoard(Position position, ChessPiece.Type type, Player player) {
		ChessPiece piece = null;
		switch (type) {
		case KING:
			piece = new King(position, player);
			break;
		case PAWN:
			piece = new Pawn(position, player);
			break;
		case KNIGHT:
			piece = new Knight(position, player);
			break;
		case QUEEN:
			piece = new Queen(position, player);
			break;
		case BISHOPE:
			piece = new Bishope(position, player);
			break;
		case ROOK:
			piece = new Rook(position, player);
			break;
		}
		field.put(position, piece);

	}

	private void initNewGame() {
		for (int x = 1; x <= 8; x++) {
			for (int y = 1; y <= 8; y++) {
				field.put(new Position(x, y), null);
			}
		}
		for (int i = 1; i <= 8; i++) {
			field.put(new Position(i, 2), new Pawn(new Position(i, 2), Player.WHITE));
			field.put(new Position(i, 7), new Pawn(new Position(i, 7), Player.BLACK));
		}

		for (int i = 1; i <= 8; i += 7) {
			field.put(new Position(i, 1), new Rook(new Position(i, 1), Player.WHITE));
			field.put(new Position(i, 8), new Rook(new Position(i, 8), Player.BLACK));
		}

		for (int i = 2; i <= 8; i += 5) {
			field.put(new Position(i, 8), new Knight(new Position(i, 8), Player.BLACK));
			field.put(new Position(i, 1), new Knight(new Position(i, 1), Player.WHITE));
		}

		for (int i = 3; i <= 8; i += 3) {
			field.put(new Position(i, 8), new Bishope(new Position(i, 8), Player.BLACK));
			field.put(new Position(i, 1), new Bishope(new Position(i, 1), Player.WHITE));
		}

		field.put(new Position(5, 1), new Queen(new Position(5, 1), Player.WHITE));
		field.put(new Position(4, 1), new King(new Position(4, 1), Player.WHITE));
		field.put(new Position(4, 8), new King(new Position(4, 8), Player.BLACK));
		field.put(new Position(5, 8), new Queen(new Position(5, 8), Player.BLACK));
	}

	public Map<Position, ChessPiece> getField() {
		return field;
	}

	public void setField(Map<Position, ChessPiece> field) {
		this.field = field;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public BaseAction getLastAction() {
		return lastAction;
	}

	public void setLastAction(BaseAction lastAction) {
		this.lastAction = lastAction;
	}

	public boolean isEnded() {
		return ended;
	}

	// TODO tweak with better structure...
	public List<ChessPiece> getChessPieces(Type type, Player player) {
		List<ChessPiece> result = new ArrayList<ChessPiece>();
		for (Position key : field.keySet()) {
			ChessPiece piece = field.get(key);
			if (piece != null && piece.getType() == type && piece.getPlayer() == player) {
				result.add(piece);
			}
		}
		return result;
	}

	@Override
	public int calculateValue(Player player) {
		return rating.rate(this, player);
	}

	// stuff for minimax algorithm
	// TODO replace by normal function..

	private ChessBoard lastBoard;

	@Override
	public void undoLastMove() {
		this.field = lastBoard.getField();
		this.lastAction = lastBoard.getLastAction();
		this.gameId = lastBoard.getGameId();
		this.hash = lastBoard.getHash();
	}

	@Override
	public boolean isGameOver() {
		// TODO implement
		return false;
	}

	@Override
	public boolean move(Move m) {
		lastBoard = cloneBoard();
		try {
			execute(new MoveAction(m, -1));
		} catch (BaseChessException e) {
			e.printStackTrace();
		}
		return true;
	}

}
