package com.scarabsoft.chess.board.rating;

import java.util.HashMap;
import java.util.Map;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.board.rating.Rating.RatingType;

public class ChessRating {

	private final Map<RatingType, Rating> ratingMap = new HashMap<Rating.RatingType, Rating>();

	public ChessRating() {
		// ratingMap.put(RatingType.MATERIAL, new MaterialRating());
		ratingMap.put(RatingType.KNIGHT, new KnightPositionRating());
		ratingMap.put(RatingType.ROOK, new RookPositionRating());
		ratingMap.put(RatingType.QUEEN, new QueenPositionRating());
		ratingMap.put(RatingType.BISHOPE, new BishopePositionRating());
		ratingMap.put(RatingType.PAWN, new PawnPositionRating());
	}

	public int rate(ChessBoard board, Player player) {
		int result = 0;
		for (RatingType type : RatingType.values()) {
			result += ratingMap.get(type).rate(board, player);
		}
		return result;
	}
}
