package com.scarabsoft.context;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableTransactionManagement
public class DataConfig {

	@Autowired
	Environment enviroment;

	@Bean(destroyMethod = "close")
	public DataSource dataSource() throws Exception {
		// for documentation see: http://www.mchange.com/projects/c3p0/
		ComboPooledDataSource dataSource = new ComboPooledDataSource();

		// connection access
		dataSource.setDriverClass(enviroment.getProperty("jdbc.driverClassName"));
		dataSource.setJdbcUrl(enviroment.getProperty("jdbc.databaseurl"));
		dataSource.setUser(enviroment.getProperty("jdbc.username"));
		dataSource.setPassword(enviroment.getProperty("jdbc.password"));

		// connection pool
		dataSource.setInitialPoolSize(20);
		dataSource.setMinPoolSize(10);
		dataSource.setMaxPoolSize(100);

		// connection age
		dataSource.setMaxIdleTime(3600);

		return dataSource;
	}

	@Bean
	public SessionFactory sessionFactory() {
		try {
			LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
			sessionFactory.setDataSource(dataSource());

			String[] mappingResources = new String[] { "hibernate/chessboard.hbm.xml", "hibernate/store.hbm.xml" };

			sessionFactory.setMappingResources(mappingResources);

			Properties hibernateProperties = new Properties();
			hibernateProperties.setProperty("hibernate.dialect", enviroment.getProperty("jdbc.dialect"));
			hibernateProperties.setProperty("hibernate.show_sql", "true");
			sessionFactory.setHibernateProperties(hibernateProperties);

			sessionFactory.afterPropertiesSet();
			return sessionFactory.getObject();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Bean
	public PlatformTransactionManager txManager() {
		return new HibernateTransactionManager(sessionFactory());
	}
}
