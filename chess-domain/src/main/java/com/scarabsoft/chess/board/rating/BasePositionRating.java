package com.scarabsoft.chess.board.rating;

import java.util.List;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.piece.ChessPiece;

public abstract class BasePositionRating implements Rating {

	protected final int[][] offset_white;

	protected final int[][] offset_black;

	protected final ChessPiece.Type type;

	public BasePositionRating(int[][] offset_white, int[][] offset_black, ChessPiece.Type type) {
		this.offset_white = offset_white;
		this.offset_black = offset_black;
		this.type = type;
	}

	public int rate(ChessBoard board, Player player) {
		int result = 0;
		List<ChessPiece> pieces = board.getChessPieces(type, player);
		for (ChessPiece piece : pieces) {
			result += ratePiece(piece.getPosition(), player);
		}
		return result;
	}

	protected int ratePiece(Position position, Player player) {
		if (player == Player.WHITE) {
			return offset_white[position.getX() - 1][position.getY() - 1];
		} else {
			return offset_black[position.getX() - 1][position.getY() - 1];
		}
	}
}
