package com.scarabsoft.chess.exception;

public abstract class BaseChessException extends Exception {

	private static final long serialVersionUID = 6478670404476795673L;

	private final String message;

	private int errorCode;

	public BaseChessException(int errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public int getErrorCode() {
		return errorCode;
	}

}
