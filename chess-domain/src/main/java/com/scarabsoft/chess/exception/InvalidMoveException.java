package com.scarabsoft.chess.exception;

public class InvalidMoveException extends BaseChessException {

	private static final long serialVersionUID = -1388735939187781754L;

	public InvalidMoveException() {
		super(ChessErrorCode.INVALID_MOVE, "You did an invalid move");

	}

}
