package com.scarabsoft.repository;

import org.hibernate.SessionFactory;

import com.scarabsoft.chess.User;

public class HibernateUserRepository extends GenericHibernateRepo<User> implements UserRepository {

	public HibernateUserRepository(SessionFactory sessionFactory) {
		super(User.class, sessionFactory);
	}

}
