package com.scarabsoft.chess.piece;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public class Knight extends ChessPiece {

	public Knight(Position position, Player player) {
		super(position, "knight", 30, player, new int[][] { { -2, 1 }, { -1, 2 }, { 1, 2 }, { 2, 1 }, { 2, -1 }, { 1, -2 }, { -1, -2 }, { -2, -1 } }, 1,
				Type.KNIGHT);
	}
}
