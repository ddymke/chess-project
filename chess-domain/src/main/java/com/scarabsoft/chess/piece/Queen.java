package com.scarabsoft.chess.piece;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public class Queen extends ChessPiece {

	public Queen(Position position, Player player) {
		super(position, "queen", 90, player, new int[][] { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, 1 }, { -1, -1 } }, 8,
				Type.QUEEN);
	}
}
