package com.scarabsoft.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.scarabsoft.chess.json.Store;

public class HibernateStoreRepository extends GenericHibernateRepo<Store> implements StoreRepository {

	public HibernateStoreRepository(SessionFactory factory) {
		super(Store.class, factory);

	}

	@Override
	public Store get(String hash) {
		Criteria criteria = createCriteria();
		criteria.add(Restrictions.eq("hash", hash));
		return (Store) criteria.uniqueResult();
	}

}
