package com.scarabsoft.chess.network.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;

import android.os.AsyncTask;
import android.os.Message;

import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.util.NetworkUtil;

public class SingleNetworkTask extends AsyncTask<Void, Void, Void> {

	private HttpUriRequest httpUriRequest;

	private HttpClient httpClient;

	private final NetworkTaskListener networkTaskListener;

	private Message resultMessage;

	private String result;

	public SingleNetworkTask(HttpUriRequest httpUriRequest, NetworkTaskListener networkTaskListener) {
		this.httpUriRequest = httpUriRequest;
		this.networkTaskListener = networkTaskListener;
		httpClient = NetworkUtil.getHttpClient();
	}

	@Override
	protected Void doInBackground(Void... params) {
		HttpResponse response = null;
		String line;
		result = "";
		try {
			response = httpClient.execute(httpUriRequest);
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((line = reader.readLine()) != null) {
				result += line;
			}
			response.getEntity().consumeContent();
			resultMessage.what = handleStatusCode(response.getStatusLine().getStatusCode());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			resultMessage.what = NetworkResult.TIME_OUT;
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		networkTaskListener.onNetworkTaskFinished(resultMessage.what);
	}

	@Override
	protected void onPreExecute() {
		resultMessage = new Message();
	}

	protected int handleStatusCode(int code) {
		if (code == HttpStatus.SC_UNAUTHORIZED) {
			return NetworkResult.INV_AUTH;
		} else if (code == NetworkResult.TIME_OUT) {
			return NetworkResult.TIME_OUT;
		} else {
			return NetworkResult.OK;
		}
	}

	public String getResult() {
		return result;
	}
}
