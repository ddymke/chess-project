package com.scarabsoft.chess.util;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.json.JsonChessBoard;
import com.scarabsoft.chess.json.Store;
import com.scarabsoft.chess.piece.ChessPiece;

public class ChessConverter {

	private static class Adapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {
		private final static String CLASSNAME = "CLASSNAME";
		private final static String INSTANCE = "INSTANCE";

		@Override
		public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
			JsonObject jsonObject = json.getAsJsonObject();
			JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
			String className = prim.getAsString();
			Class<?> clazz = null;
			try {
				clazz = Class.forName(className);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new JsonParseException(e.getMessage());
			}
			return context.deserialize(jsonObject.get(INSTANCE), clazz);
		}

		@Override
		public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
			JsonObject retValue = new JsonObject();
			String className = src.getClass().getCanonicalName();
			retValue.addProperty(CLASSNAME, className);
			JsonElement elem = context.serialize(src);
			retValue.add(INSTANCE, elem);
			return retValue;
		}

	}

	private final static Gson gson = new GsonBuilder().registerTypeAdapter(ChessPiece.class, new Adapter<ChessPiece>())
			.registerTypeAdapter(BaseAction.class, new Adapter<BaseAction>()).serializeNulls().create();

	public static BaseAction toAction(String json) {
		return gson.fromJson(json, BaseAction.class);
	}

	public static String fromAction(BaseAction action) {
		return gson.toJson(action, BaseAction.class);
	}

	public static String fromStore(Store s) {
		return gson.toJson(s, Store.class);
	}

	public static Store toStore(String json) {
		return gson.fromJson(json, Store.class);
	}

	public static Store ChessBoardToStore(ChessBoard cb, String player) {
		final Store result = new Store();
		result.setBestAction(fromAction(cb.getLastAction()));
		result.setHash(cb.getHash());
		result.setContent(fromField(cb.getField()));
		result.setPlayer(player);
		return result;
	}

	public static ChessBoard StoreToChessboard(int gameId, Store store) {
		final ChessBoard result = new ChessBoard();
		result.setLastAction(toAction(store.getBestAction()));
		result.setGameId(gameId);
		result.setField(toField(store.getContent()));
		result.setHash(store.getHash());
		return result;
	}

	public static JsonChessBoard toJsonChessBoad(ChessBoard cb) {
		JsonChessBoard result = new JsonChessBoard();
		result.setHash(cb.getHash());
		result.setGameId(cb.getGameId());
		result.setContent(fromField(cb.getField()));
		result.setLastAction(fromAction(cb.getLastAction()));
		return result;
	}

	public static ChessBoard fromJsonChessBoard(JsonChessBoard jb) {
		ChessBoard result = new ChessBoard();
		result.setGameId(jb.getGameId());
		result.setField(toField(jb.getContent()));
		result.setLastAction(ChessConverter.toAction(jb.getLastAction()));
		return result;
	}

	public static String fromChessPiece(ChessPiece piece) {
		return gson.toJson(piece, ChessPiece.class);
	}

	public static ChessPiece toChessPiece(String json) {
		return gson.fromJson(json, ChessPiece.class);
	}

	public static String fromField(Map<Position, ChessPiece> field) {
		Map<Position, String> temp = new HashMap<Position, String>();
		for (Position key : field.keySet()) {
			temp.put(key, gson.toJson(field.get(key), ChessPiece.class));
		}
		return gson.toJson(temp);
	}

	public static Map<Position, ChessPiece> toField(String json) {
		Map<String, String> temp = gson.fromJson(json, new TypeToken<Map<String, String>>() {
		}.getType());

		Map<Position, ChessPiece> result = new HashMap<Position, ChessPiece>();
		for (String key : temp.keySet()) {
			result.put(new Position(key), gson.fromJson(temp.get(key), ChessPiece.class));
		}
		return result;
	}

}
