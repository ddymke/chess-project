package com.scarabsoft.repository;

import com.scarabsoft.chess.User;

public interface UserRepository {

	public void save(User user);

	public void update(User user);

	public User get(int id);

}
