package com.scarabsoft.chess.util;

import java.security.KeyStore;

import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import com.scarabsoft.chess.network.BaseNetworkActivity;

public class NetworkUtil {

	public static HttpPost generatePost(String url, MultipartEntity entity) {
		HttpPost result = new HttpPost(BaseNetworkActivity.HOST + url);
		result.setEntity(entity);
		return result;
	}

	public static HttpUriRequest generateGet(String url, String params) {
		System.out.println("CALL:" + BaseNetworkActivity.HOST + url + params);
		HttpGet get = new HttpGet(BaseNetworkActivity.HOST + url + params);
		return get;
	}

	// public static HttpPost generatePost(String url, String token,
	// MultipartEntity entity) {
	// HttpPost result = new HttpPost(BaseNetworkActivity.HOST + url);
	// if (token != null) {
	// result.addHeader("a token", token);
	// }
	// if (entity != null) {
	// result.setEntity(entity);
	// }
	// return result;
	// }

	// TODO change to getDefaultHttpClient for release
	public static DefaultHttpClient getHttpClient() {
		return getUSAFEHttpClient();
	}

	private static DefaultHttpClient getDefaultHttpClient() {
		DefaultHttpClient result = null;

		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

		ClientConnectionManager ccm = new ThreadSafeClientConnManager(getParams(), registry);

		result = new DefaultHttpClient(ccm, getParams());

		return result;
	}

	private static HttpParams getParams() {
		HttpParams result = new BasicHttpParams();
		HttpProtocolParams.setVersion(result, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(result, HTTP.UTF_8);

		HttpConnectionParams.setStaleCheckingEnabled(result, false);
		HttpConnectionParams.setSocketBufferSize(result, 8192);

		HttpConnectionParams.setConnectionTimeout(result, 800000);
		HttpConnectionParams.setSoTimeout(result, 800000);
		HttpProtocolParams.setVersion(result, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(result, HTTP.DEFAULT_CONTENT_CHARSET);
		HttpProtocolParams.setUseExpectContinue(result, true);
		return result;
	}

	private static DefaultHttpClient getUSAFEHttpClient() {
		try {

			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new FakeFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(getParams(), registry);

			return new DefaultHttpClient(ccm, getParams());

		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

}
