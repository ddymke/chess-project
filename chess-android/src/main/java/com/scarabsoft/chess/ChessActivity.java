package com.scarabsoft.chess;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.action.CreateGameAction;
import com.scarabsoft.chess.action.MoveAction;
import com.scarabsoft.chess.adapter.ChessAdapter;
import com.scarabsoft.chess.ai.Ave;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.database.ChessSource;
import com.scarabsoft.chess.domain.ChessIPC;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.json.JsonChessBoard;
import com.scarabsoft.chess.network.CreateGameNetworkActivity;
import com.scarabsoft.chess.network.LastActionNetworkActivity;
import com.scarabsoft.chess.network.SendActionNetworkActivity;
import com.scarabsoft.chess.network.domain.NetworkCommand;
import com.scarabsoft.chess.network.domain.NetworkResult;
import com.scarabsoft.chess.piece.ChessPiece;
import com.scarabsoft.chess.util.ChessConverter;
import com.scarabsoft.chess.util.ChessUtil;

public class ChessActivity extends Activity implements OnItemClickListener {

	private int gameId;

	private GridView chessView;

	private ChessAdapter chessAdapter;

	private Timer timer;

	private boolean playerTurn = true;

	private ChessPiece selected;

	private Position selectedPosition;

	private ChessBoard chessBoard;

	private boolean istOnline = true;

	private Player player = Player.WHITE;

	private ChessSource source;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chess);
		chessView = (GridView) findViewById(R.id.chessboard);
		Ave.init();

		source = new ChessSource(this);
		chessView.setOnItemClickListener(this);

		Intent intent = null;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ChessActivity.this);
		if ((gameId = Integer.parseInt(preferences.getString(ChessIPC.GAMEID, "-1"))) == -1) {
			if (istOnline) {
				intent = new Intent(this, CreateGameNetworkActivity.class);
				startActivityForResult(intent, NetworkCommand.CREATE_GAME);
			} else {
				// generate a local game..
				chessBoard = new ChessBoard();
				execute(new CreateGameAction());
				chessBoard.setGameId(-1);
				source.insert(ChessConverter.toJsonChessBoad(chessBoard));
			}
		} else {
			// load old game from db and synchronize
			chessBoard = ChessConverter.fromJsonChessBoard(source.getLastChessBoard(gameId));
			drawField(chessBoard.getField());
			// TODO sync with server
			if (chessBoard.getLastAction().getPlayer() != player) {
				playerTurn = true;
			} else {
				calculateLocal();
			}
		}

	}

	private void saveGameId(int gameId) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ChessActivity.this);
		final Editor editor = preferences.edit();
		editor.putString(ChessIPC.GAMEID, String.valueOf(gameId));
		editor.commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		BaseAction action;
		if (NetworkResult.OK == resultCode) {
			if (requestCode == NetworkCommand.CREATE_GAME) {
				JsonChessBoard jBoard = ChessUtil.toJsonChessBoard(data.getExtras().getString(ChessIPC.RESULT));
				gameId = jBoard.getGameId();
				// saveGameId(gameId);
				chessBoard = ChessConverter.fromJsonChessBoard(jBoard);
				drawField(chessBoard.getField());
				if (player == Player.WHITE) {
					playerTurn = true;
				}
			} else if (requestCode == NetworkCommand.LAST_ACTION) {
				action = ChessConverter.toAction(data.getExtras().getString(ChessIPC.RESULT));
				if (action.getPlayer() != player) {
					timer.cancel();
					execute(action);
					source.insert(ChessConverter.toJsonChessBoad(chessBoard));
					playerTurn = true;
				}
			} else if (requestCode == NetworkCommand.ACTION) {
				action = ChessConverter.toAction(data.getExtras().getString(ChessIPC.RESULT));
				execute(action);
				source.insert(ChessConverter.toJsonChessBoad(chessBoard));
				startTimer();
			} else if (requestCode == NetworkCommand.SYNC) {

			}

		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			startActivity(new Intent(this, PreferenceActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private void drawField(Map<Position, ChessPiece> map) {
		chessAdapter = new ChessAdapter(this, map);
		chessView.setAdapter(chessAdapter);
		chessAdapter.notifyDataSetChanged();
	}

	private void calculateLocal() {
		BaseAction bestMove = Ave.getInstance().calculateBestAction(gameId, chessBoard, (player == Player.WHITE) ? Player.BLACK : Player.WHITE);
		execute(bestMove);
		source.insert(ChessConverter.toJsonChessBoad(chessBoard));
		playerTurn = true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (playerTurn) {
			ChessPiece piece = (ChessPiece) chessAdapter.getItem(position);
			if (piece != null && selected != null && piece.getPlayer() == selected.getPlayer()) {
				selected = piece;
				selectedPosition = new Position(position);
			}
			if (selected == null) {
				selected = piece;
				selectedPosition = new Position(position);
			} else {
				try {
					if (chessBoard.getPossiblePositions(selected, selectedPosition).contains(new Position(position))) {
						MoveAction action = new MoveAction(new Move(selectedPosition, new Position(position), selected), gameId);
						if (istOnline) {
							Intent intent = new Intent(this, SendActionNetworkActivity.class);
							intent.putExtra(ChessIPC.ACTION, ChessConverter.fromAction(action));
							intent.putExtra(ChessIPC.GAMEID, gameId);
							startActivityForResult(intent, NetworkCommand.ACTION);
						} else {
							execute(new MoveAction(new Move(selectedPosition, new Position(position), selected), gameId));
							calculateLocal();
						}
						selected = null;
						selectedPosition = null;
					} else {
						Toast.makeText(this, "invalid move", Toast.LENGTH_LONG).show();
					}
				} catch (BaseChessException e) {
					e.printStackTrace();
				}
			}
		} else {
			Toast.makeText(this, "Wait for computer action", Toast.LENGTH_LONG).show();
		}
	}

	private void execute(BaseAction action) {
		try {
			chessBoard.execute(action);
		} catch (BaseChessException e) {
			e.printStackTrace();
		}
		drawField(chessBoard.getField());
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (timer != null)
			timer.cancel();
	}

	private void getLastAction() {
		final Intent intent = new Intent(this, LastActionNetworkActivity.class);
		intent.putExtra(ChessIPC.GAMEID, gameId);
		startActivityForResult(intent, NetworkCommand.LAST_ACTION);
	}

	private void startTimer() {
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				getLastAction();
			}
		}, 500, 6000);
	}
}
