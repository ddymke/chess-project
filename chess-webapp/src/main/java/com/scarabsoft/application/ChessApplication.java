package com.scarabsoft.application;

import com.scarabsoft.chess.action.BaseAction;
import com.scarabsoft.chess.exception.BaseChessException;
import com.scarabsoft.chess.json.JsonChessBoard;

public interface ChessApplication {

	public JsonChessBoard createNewGame();

	public JsonChessBoard getChessBoard(int gameId);

	public BaseAction execute(int gameId, BaseAction action) throws BaseChessException;

}
