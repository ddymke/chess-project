package com.scarabsoft.chess.board.rating;

import com.scarabsoft.chess.piece.ChessPiece.Type;

public class QueenPositionRating extends BasePositionRating {

	private final static int[][] offset_white = { //
	{ 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 4, 5, 5, 4, 0, 0 }, //
			{ 0, 2, 4, 10, 10, 4, 2, 0 },//
			{ 0, 2, 10, 12, 12, 10, 2, 0 },//
			{ -10, 2, 10, 12, 12, 10, 2, -10 },//
			{ -10, -10, 4, 10, 10, 4, -10, -10 },//
			{ -10, 2, 8, 8, 8, 8, 2, -10 },//
			{ -10, -8, 0, 0, 0, 0, -8, -10 } };

	private final static int[][] offset_black = {//
	{ -10, -8, 0, 0, 0, 0, -8, -10 },//
			{ -10, 2, 8, 8, 8, 8, 2, -10 },//
			{ -10, -10, 4, 10, 10, 4, -10, -10 },//
			{ -10, 2, 10, 12, 12, 10, 2, -10 },//
			{ 0, 2, 10, 12, 12, 10, 2, 0 },//
			{ 0, 2, 4, 10, 10, 4, 2, 0 },//
			{ 0, 0, 4, 5, 5, 4, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0 } };

	public QueenPositionRating() {
		super(offset_white, offset_black, Type.QUEEN);
	}
}
