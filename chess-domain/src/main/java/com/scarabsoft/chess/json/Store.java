package com.scarabsoft.chess.json;

public class Store {

	private int id;

	private String hash;

	private String content;

	private String bestAction;

	private String player;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getBestAction() {
		return bestAction;
	}

	public void setBestAction(String bestAction) {
		this.bestAction = bestAction;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}
}
