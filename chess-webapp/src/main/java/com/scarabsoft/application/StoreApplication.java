package com.scarabsoft.application;

import com.scarabsoft.chess.json.Store;

public interface StoreApplication {

	public Store get(String hash);

}
