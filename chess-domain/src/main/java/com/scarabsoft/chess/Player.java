package com.scarabsoft.chess;

public enum Player {

	WHITE(0), BLACK(1);

	private int value;

	public int toInt() {
		return value;
	}

	private Player(int value) {
		this.value = value;
	}

	public static Player toEnum(int value) {
		switch (value) {
		case 0:
			return WHITE;
		case 1:
			return BLACK;
		default:
			return null;
		}
	}

	public String toString() {
		if (value == 0) {
			return "white";
		}
		return "black";
	}

}
