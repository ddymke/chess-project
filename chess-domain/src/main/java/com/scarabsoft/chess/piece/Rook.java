package com.scarabsoft.chess.piece;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;

public class Rook extends ChessPiece {

	public Rook(Position position, Player player) {
		super(position, "rook", 50, player, new int[][] { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } }, 1, Type.ROOK);
	}
}
