package com.scarabsoft.chess;

public class Formatter {

	public static String format(Move move, int round, boolean killMove) {
		String result = "";
		String figure = "";
		switch (move.getChessPiece().getType()) {
		case PAWN:
			figure = "";
			break;
		case KNIGHT:
			figure = "N";
			break;
		case BISHOPE:
			figure = "B";
			break;
		case ROOK:
			figure = "R";
			break;
		case QUEEN:
			figure = "Q";
			break;
		case KING:
			figure = "K";
			break;
		}

		String position = move.getDest().toString().toLowerCase();

		if (killMove) {
			result += figure + "x" + position;
		} else {
			result += figure + position;
		}

		return result;
	}
}
