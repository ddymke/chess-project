package com.scarabsoft.chess.board.rating;

import java.util.Map;

import com.scarabsoft.chess.Player;
import com.scarabsoft.chess.Position;
import com.scarabsoft.chess.board.ChessBoard;
import com.scarabsoft.chess.piece.ChessPiece;

public class MaterialRating implements Rating {

	@Override
	public int rate(ChessBoard board, Player player) {
		int result = 0;
		Map<Position, ChessPiece> map = board.getChessPieces(player);
		for (Position key : map.keySet()) {
			final ChessPiece piece = map.get(key);
			result += piece.getValue();
		}
		System.out.println(result);
		return result;

	}

}
